## About the stack

Before you start using the tools you just installed, it's a good idea to understand each of the components and how they interact with one another.

* nodeos (node + eos = nodeos) - the core EOSIO node daemon that can be configured with plugins to run a node. Example uses are block production, dedicated API endpoints, and local development.

## Requirements

* python3-saml - 
* python 3.7.x - 

## Installation

1. Install 

```bash
# For the most recent stable version
$ npm install mithril --save
# For the most recent unstable version
$ npm install mithril@next --save
```

2. Step 

```bash
# For the most recent stable version
$ npm install mithril --save
# For the most recent unstable version
$ npm install mithril@next --save
```

3. Step
